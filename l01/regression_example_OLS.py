import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import statsmodels.api as sm

df = pd.read_excel("data/consumption_portugal.xlsx")
print(df)

df = pd.DataFrame(df, columns=['cons'])
plt.plot(df)
plt.show()

x = np.arange(len(df))
print(x)
y = np.array(df)
X = sm.add_constant(x)  # adding a column of 1s before x
print(X)

model = sm.OLS(y, X)  # regression with ordinary least squares
res = model.fit()
print(res.summary())

'''
The dependent coefficient x1 is 576.
This means that for each timestep, the consumption increases by 576.

R-squared is 0.803, which means that 80% of the results is explained by the tendency. 
 '''