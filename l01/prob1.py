'''
Problem 1
The Airline Passengers dataset describes the total number of airline passengers
in US from 1949 to 1960 (monthly observations in thousands).
Source:https://raw.githubusercontent.com/jbrownlee/Datasets/master/airline-
passengers.csv
a) Import data to Python. Plot the time series. Are there any seasonal
fluctuations?
b) Use multiplicative decomposition to estimate the trend-cycle, seasonal
indices and error component.
'''

import pandas as pd
import matplotlib.pyplot as plt
from statsmodels.tsa.seasonal import seasonal_decompose

# url = 'https://raw.githubusercontent.com/jbrownlee/Datasets/master/airline-passengers.csv'
url = 'data/airline-passengers.csv'  # local path

df = pd.read_csv(url, index_col='Month', parse_dates=True)
plt.plot(df)
plt.show()

'''
We can see a tendency and seasonality on the time series.
The time series' features are increasing over time, so
we'll use the multiplicative model.
'''

# compute trend cycle
s = 12
s_component = seasonal_decompose(df, model='multiplicative', period=12)
s_component.plot()
plt.show()