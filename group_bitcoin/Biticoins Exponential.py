# -*- coding: utf-8 -*-
"""
Created on Thu May 12 12:28:52 2022

@author: joaop
"""

import pandas as pd
import numpy as np
from statsmodels.tsa.holtwinters import ExponentialSmoothing
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_percentage_error 
from sklearn.metrics import mean_absolute_error


url ="C:/Users/joaop/OneDrive/Ambiente de Trabalho/TSF/bitcoins_monthly_2.csv"
df = pd.read_csv(url)
df = pd.read_csv(url, index_col='month',parse_dates=True)
df.index.freq = 'MS'
print(df)

#Train and Test data Splitting 80- 20 
train_data = df.iloc[:73-18]
test_data = df.iloc[73-18:]

# Holt Winters multiplicative method
hwa_model = ExponentialSmoothing(train_data['close_avg'],
                                 trend='add',
                                 seasonal='multiplicative',
                                 seasonal_periods=12).fit()
hwa_test_pred = hwa_model.forecast(36).rename('HWA close_avg Forecast')
#print(hwa_test_pred)

train_data['close_avg'].plot(legend=True,label='Train')
test_data['close_avg'].plot(legend=True,label='Test',figsize=(12,8))
hwa_test_pred.plot(legend=True,label='HWA close_avg Forecast')

# RMSE (HWA)
rmse_hwa = round(np.sqrt(mean_squared_error(test_data['close_avg'], hwa_test_pred)),2)
print("RMSE_HWA is ",rmse_hwa)
mae_hwa = round(mean_absolute_error(test_data['close_avg'],hwa_test_pred),2)
print("MAE_HWA is ",mae_hwa)
mape_hwa = round(100*mean_absolute_percentage_error(test_data['close_avg'],hwa_test_pred),2)
print("MAPE_HWA is ",mape_hwa,'%')

# Holt Winters additive method

hwa_model = ExponentialSmoothing(train_data['close_avg'],
                                 trend='add',
                                 seasonal='additive',
                                 seasonal_periods=12).fit()
hwa_test_pred = hwa_model.forecast(36).rename('HWA close_avg Forecast')
#print(hwa_test_pred)

train_data['close_avg'].plot(legend=True,label='Train')
test_data['close_avg'].plot(legend=True,label='Test',figsize=(12,8))
hwa_test_pred.plot(legend=True,label='HWA close_avg Forecast')

# RMSE (HWA)
rmse_hwa = round(np.sqrt(mean_squared_error(test_data['close_avg'], hwa_test_pred)),2)
print("RMSE_HWA is ",rmse_hwa)
mae_hwa = round(mean_absolute_error(test_data['close_avg'],hwa_test_pred),2)
print("MAE_HWA is ",mae_hwa)
mape_hwa = round(100*mean_absolute_percentage_error(test_data['close_avg'],hwa_test_pred),2)
print("MAPE_HWA is ",mape_hwa,'%')


# Forecast the next 3 years using the best model
final_model = ExponentialSmoothing(df['close_avg'],
                                   trend='add',
                                   seasonal='multiplicative',
                                   seasonal_periods=12).fit()
predictions = final_model.forecast(36).rename('Forecast')
df['close_avg'].plot(legend=True,label='close_avg')
predictions.plot(legend=True,label='Forecast')