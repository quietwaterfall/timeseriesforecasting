# -*- coding: utf-8 -*-
"""
Created on Thu May 12 11:32:43 2022

@author: joaop
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from statsmodels.tsa.stattools import adfuller
import statsmodels.api as sm  
from statsmodels.tsa.stattools import acf  
from statsmodels.tsa.stattools import pacf


url ="C:/Users/joaop/OneDrive/Ambiente de Trabalho/TSF/bitcoins_monthly_2.csv"
df = pd.read_csv(url)

df = pd.read_csv(url, index_col='month',parse_dates=True)
print(df)
df.index.freq = 'MS'
print(df)
plt.figure(figsize=(12,8))
plt.plot(df.close_avg)

#Decomposition
decomposition = sm.tsa.seasonal_decompose(df.close_avg,model='multiplicative')
plt.rcParams["figure.figsize"] = [16,8]
fig = decomposition.plot()

# Compute Sample ACF and Sample PACF
fig0 = plt.figure(figsize=(12,8))
ax1 = fig0.add_subplot(211)
fig0 = sm.graphics.tsa.plot_acf(df.close_avg, lags=40, ax=ax1)
ax2 = fig0.add_subplot(212)
fig0 = sm.graphics.tsa.plot_pacf(df.close_avg, lags=40, ax=ax2)

# Unit root tests
def test_stationarity(timeseries):
       
    #Perform Dickey-Fuller test:
    print('Results of Dickey-Fuller Test:')
    dftest = adfuller(timeseries, autolag='AIC')
    dfoutput = pd.Series(dftest[0:4], 
                         index=['Test Statistic','p-value',
                                '#Lags Used','Number of Observations Used'])
    for key,value in dftest[4].items():
        dfoutput['Critical Value (%s)'%key] = value
    print(dfoutput)
        
test_stationarity(df.close_avg)  


# Log of close_avg
df.close_avg_log= df.close_avg.apply(lambda x: np.log(x))  
test_stationarity(df.close_avg_log)
df.close_avg_log.plot(figsize=(12,8), title= 'Logarithm of close_avg', fontsize=14)

# First differences
df['first_difference'] = df.close_avg - df.close_avg.shift(1)  
test_stationarity(df.first_difference.dropna(inplace=False))
df['first_difference'].plot(figsize=(12,8), 
                            title= 'First difference', fontsize=14)

# Differences of Log
df['log_first_difference'] = df.close_avg_log - df.close_avg_log.shift(1)  
test_stationarity(df.log_first_difference.dropna(inplace=False))


# Sesaonal Differences
df['seasonal_difference'] = df.close_avg - df.close_avg.shift(12)  
test_stationarity(df.seasonal_difference.dropna(inplace=False))
df['seasonal_difference'].plot(figsize=(12,8), 
                               title= 'Sesaonal difference', fontsize=14)


# Ordinary and sesaonal differences
df['seasonal_first_difference'] = df.first_difference - df.first_difference.shift(12)  
test_stationarity(df.seasonal_first_difference.dropna(inplace=False))
df['seasonal_first_difference'].plot(figsize=(12,8), 
                                     title= 'Ordinary and seasonal differences', fontsize=14)


# Ordinary and sesaonal differences of logs
df['log_seasonal_first_difference'] = df.log_first_difference - df.log_first_difference.shift(12)  
test_stationarity(df.log_seasonal_first_difference.dropna(inplace=False))
df['log_seasonal_first_difference'].plot(figsize=(12,8), 
                                     title= 'Ordinary and seasonal differences of logarithm of close_avg', fontsize=14)

# ACF and PACF of stationary time series: Ordinary and sesaonal differences
# Do not include the first observation since we had to apply the first difference 
fig = plt.figure(figsize=(12,8))
ax1 = fig.add_subplot(211)
fig = sm.graphics.tsa.plot_acf(df.log_first_difference.iloc[1:], 
                               lags=38, ax=ax1)
ax2 = fig.add_subplot(212)
fig = sm.graphics.tsa.plot_pacf(df.log_first_difference.iloc[1:],
                                lags=38, ax=ax2)


model1 = sm.tsa.statespace.SARIMAX(df.close_avg, trend='n', 
                                order=(0,1,0), 
                                seasonal_order=(0,1,1,12))
results1 = model1.fit()
print(results1.summary())


model2 = sm.tsa.statespace.SARIMAX(df.close_avg, trend='n', 
                                order=(0,1,0), 
                                seasonal_order=(1,1,1,12))
results2 = model2.fit()
print(results2.summary())

model3 = sm.tsa.statespace.SARIMAX(df.close_avg, trend='n', 
                                order=(0,1,0), 
                                seasonal_order=(0,1,1,20))
results3 = model3.fit()
print(results3.summary())


model4 = sm.tsa.statespace.SARIMAX(df.close_avg, trend='n', 
                                order=(0,1,0), 
                                seasonal_order=(1,1,1,20))
results4 = model4.fit()
print(results4.summary())

from statsmodels.tsa.arima_model import ARIMA


model5 = ARIMA(df.close_avg,order=(0, 1, 0))

results5 = model5.fit()
print(results5.summary())





from statsmodels.tsa.statespace.sarimax import SARIMAX
from tqdm import tqdm_notebook
from tqdm.notebook import trange, tqdm
from itertools import product

def optimize_SARIMA(parameters_list, d, D, s, exog):
    """
        Return dataframe with parameters, corresponding AIC and SSE
        
        parameters_list - list with (p, q, P, Q) tuples
        d - integration order
        D - seasonal integration order
        s - length of season
        exog - the exogenous variable
    """
    
    results = []
    
    for param in tqdm_notebook(parameters_list):
        try: 
            model = SARIMAX(exog, order=(param[0], d, param[1]), seasonal_order=(param[2], D, param[3], s)).fit(disp=-1)
        except:
            continue
            
        aic = model.aic
        results.append([param, aic])
        
    result_df = pd.DataFrame(results)
    result_df.columns = ['(p,q)x(P,Q)', 'AIC']
    #Sort in ascending order, lower AIC is better
    result_df = result_df.sort_values(by='AIC', ascending=True).reset_index(drop=True)
    
    return result_df

p = range(0, 4, 1)
d = 1
q = range(0, 4, 1)
P = range(0, 4, 1)
D = 1
Q = range(0, 4, 1)
s = 4
parameters = product(p, q, P, Q)
parameters_list = list(parameters)
print(len(parameters_list))
result_df = optimize_SARIMA(parameters_list, 1, 1, 4, df['close_avg'])
result_df


def optimize_SARIMA(parameters_list, d, D, s, exog):
    """
        Return dataframe with parameters, corresponding AIC and SSE
        
        parameters_list - list with (p, q, P, Q) tuples
        d - integration order
        D - seasonal integration order
        s - length of season
        exog - the exogenous variable
    """
    
    results = []
    
    for param in tqdm_notebook(parameters_list):
        try: 
            model = SARIMAX(exog, order=(param[0], d, param[1]), seasonal_order=(param[2], D, param[3], s)).fit(disp=-1)
        except:
            continue
            
        aic = model.aic
        results.append([param, aic])
        
    result_df = pd.DataFrame(results)
    result_df.columns = ['(p,q)x(P,Q)', 'AIC']
    #Sort in ascending order, lower AIC is better
    result_df = result_df.sort_values(by='AIC', ascending=True).reset_index(drop=True)
    
    return result_df

p = range(0, 4, 1)
d = 1
q = range(0, 4, 1)
P = range(0, 4, 1)
D = 1
Q = range(0, 4, 1)
s = 12
parameters = product(p, q, P, Q)
parameters_list = list(parameters)
print(len(parameters_list))
result_df = optimize_SARIMA(parameters_list, 1, 1, 4, df['close_avg'])
result_df


