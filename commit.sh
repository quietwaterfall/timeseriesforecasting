#/bin/bash
cd $1 $2

for i in $(find $(pwd) -name '*.ipynb' -not -path "*/lib/*" -not -path "*ipynb_checkpoints*");
do
    jupyter nbconvert --ClearOutputPreprocessor.enabled=True --inplace $i
    git add $i
done;


git commit
git push


